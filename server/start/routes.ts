/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return 'dastkhat home'
})
Route.post('/login','UsersController.login')
Route.post('/logout','UsersController.logout')
Route.post("/users/forget", "UsersController.forgetPass");
Route.post("/users/reset", "UsersController.resetPass");


Route.group(() => {
    Route.post('entities/add', 'EntitiesController.create')
    Route.delete('entities/delete', 'EntitiesController.destroy')
    Route.put('entities/update', 'EntitiesController.update')
}).middleware(['admin'])

Route.get('entities/id', 'EntitiesController.show')
Route.get('entities', 'EntitiesController.index')
Route.get('entities/image', 'EntitiesController.getImage')
Route.get('entities/index', 'EntitiesController.getImageIndex')
Route.get('entities/album', 'EntitiesController.getImageAlbum')


Route.group(() => {
  Route.get("/users","UsersController.index")
  Route.delete("/users/delete/id", "UsersController.destroy");
}).middleware(['admin'])

Route.post("/users/add","UsersController.create");
Route.get("/users/id", "UsersController.show");
Route.put("/users/update/id", "UsersController.update");
Route.get('/users/avatar', 'UsersController.getImage')

Route.group(() => {
  Route.post("/categories/add","CategoriesController.create");
  Route.get("/categories/id", "CategoriesController.show");
  Route.put("/categories/update/id", "CategoriesController.update");
  Route.delete("/categories/delete/id", "CategoriesController.destroy");


  Route.post("/tags/add","tagsController.create");
  Route.get("/tags/id", "tagsController.show");
  Route.put("/tags/update/id", "tagsController.update");
  Route.delete("/tags/delete/id", "tagsController.destroy");
}).middleware(['admin'])


