declare module '@ioc:Adonis/Addons/Auth' {
  import Tokens from "App/Models/Tokens";
  import User from "App/Models/User";

  interface ProvidersList {
    user: {
      implementation: LucidProviderContract<typeof User>,
      config: LucidProviderConfig<typeof User>,
    },
    apps: {
      implementation: LucidProviderContract<typeof User>,
      config: LucidProviderConfig<typeof User>,
    }
  }

  interface GuardsList {
    web: {
      implementation: SessionGuardContract<'user', 'web'>,
      config: SessionGuardConfig<'user'>,
    },
    api: {
      implementation: OATGuardContract<'apps', 'api'>,
      config: OATGuardConfig<'apps'>,
    }
  }
}
