import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AttachRelations extends BaseSchema {
  protected tableName = 'attach_relations'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('attach_relation_id')
      table.integer('attach_id').index('attach_id').unsigned().references('id').inTable('attaches')
      table.integer('entity_id').index('entity_id').unsigned().references('id').inTable('entities')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
