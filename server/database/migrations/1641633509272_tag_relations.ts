import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TagRelations extends BaseSchema {
  protected tableName = 'tag_relations'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('tag_relation_id')
      table.integer('tag_id').index('tag_id').unsigned().references('id').inTable('tags')
      table.integer('entity_id').index('entity_id').unsigned().references('id').inTable('entities')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
