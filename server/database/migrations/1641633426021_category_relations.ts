import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CategoryRelations extends BaseSchema {
  protected tableName = 'category_relations'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('category_relation_id')
      table.integer('category_id').index('category_id').unsigned().references('id').inTable('categories')
      table.integer('entity_id').index('entity_id').unsigned().references('id').inTable('entities')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
