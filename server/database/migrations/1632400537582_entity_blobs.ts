import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class EntityBlobs extends BaseSchema {
  protected tableName = 'entity_blobs'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('id')
      table.integer('entity_id').index('entity_id').unique().unsigned().references('entities.id')
      table.text('summary')
      table.text('body')
      table.text('url_target')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
