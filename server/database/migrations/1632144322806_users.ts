import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('id')
      table.string('name').notNullable()
      table.string('lastname').notNullable()
      table.string('fullname')
      table.string('email').index('email').notNullable().unique()
      table.string('username').index('username').notNullable().unique()
      table.bigInteger('mobile').index('mobile').notNullable().unique()
      table.string('password').notNullable()
      table.string('remember_me_token')
      table.string('forget_code',10)
      table.dateTime('forget_sent_at')
      table.integer('gender').notNullable().defaultTo(0)
      table.string('attach_token',255)
      table.string('member_attach_ext').index('member_attach_ext')
      table.integer('member_img_width',5)
      table.integer('member_img_height',5)
      table.string('member_img_type',20).index('member_img_type')
      table.integer('member_file_size',11).index('member_file_size')
      table.string('member_attach_name')
      table.boolean('is_deleted').notNullable().defaultTo(false).index('is_deleted')
      table.enu('role', ['admin', 'member']).notNullable().defaultTo('member').index('role')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
