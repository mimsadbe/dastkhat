import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Entities extends BaseSchema {
  protected tableName = 'entities'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('id')
      table.string('title').index('title').unique()
      table.string('subtitle').index('subtitle')
      table.string('index_attach_id').index('index_attach_id')
      table.integer('user_id').index('user_id').unsigned().references('users.id')
      table.integer('priority').index('priority')
      table.integer('visit_count').index('visit_count')
      table.timestamp('last_visit_date')
      table.boolean('is_confirmed').index('is_confirmed')
      table.boolean('is_deleted').index('is_deleted').defaultTo(0)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
