import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Attaches extends BaseSchema {
  protected tableName = 'attaches'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.engine('InnoDB')
      table.increments('id')
      table.string('attach_token',255)
      table.integer('user_id').index('user_id')
      table.string('attach_title').index('attach_title')
      table.string('attach_ext').index('attach_ext')
      table.integer('attach_img_width',5)
      table.integer('attach_img_height',5)
      table.string('attach_img_type',20).index('attach_img_type')
      table.integer('attach_file_size',11).index('attach_file_size')
      table.string('attach_original_name')
      table.integer('attach_priority')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
