import { DateTime } from 'luxon'
import {BaseModel, column, belongsTo, BelongsTo, manyToMany, ManyToMany} from '@ioc:Adonis/Lucid/Orm'
import EntityBlob from "App/Models/EntityBlob";
import Attach from "App/Models/Attach";
import Category from "App/Models/Category";
import Tag from "App/Models/Tag";

export default class Entity extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public title: string

  @column({})
  public subtitle: string

  @column({})
  public index_attach_file: number

  @column({})
  public index_attach_id: number

  @column({})
  public user_id: number

  @column({})
  public priority: number

  @column({})
  public visit_count: number

  @column({})
  public last_visit_date: DateTime

  @column({})
  public is_confirmed: boolean

  @column({})
  public is_deleted: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => EntityBlob, {
    foreignKey: 'id',
  })
  public blob: BelongsTo<typeof EntityBlob>

  @manyToMany(() => Attach, {
    localKey: 'id',
    pivotForeignKey: 'entity_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'attach_id',
    pivotTable: 'attach_relations',
  })
  public files: ManyToMany<typeof Attach>

  @manyToMany(() => Category, {
    localKey: 'id',
    pivotForeignKey: 'entity_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'category_id',
    pivotTable: 'category_relations',
  })
  public category: ManyToMany<typeof Category>

  @manyToMany(() => Tag, {
    localKey: 'id',
    pivotForeignKey: 'entity_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'tag_id',
    pivotTable: 'tag_relations',
  })
  public tag: ManyToMany<typeof Tag>

}
