import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CategoryRelation extends BaseModel {
  @column({ isPrimary: true })
  public category_relation_id: number

  @column({})
  public category_id: number

  @column({})
  public entity_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
