import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class TagRelation extends BaseModel {
  @column({ isPrimary: true })
  public tag_relation_id: number

  @column({})
  public tag_id: number

  @column({})
  public entity_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
