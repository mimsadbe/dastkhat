import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Usertoken extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public member_id: number

  @column({})
  public token: string

  @column({})
  public is_login: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime
}
