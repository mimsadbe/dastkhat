import { DateTime } from 'luxon'
import {BaseModel, beforeSave, column} from '@ioc:Adonis/Lucid/Orm'
import Hash from "@ioc:Adonis/Core/Hash";

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public name: string

  @column({})
  public lastname: string

  @column({})
  public fullname: string

  @column({})
  public email: string

  @column({})
  public username: string

  @column({})
  public mobile: number

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public forget_code?: string

  @column()
  public forget_sent_at?: DateTime

  @column({})
  public gender: number

  @column({})
  public attach_token: string

  @column({})
  public member_attach_ext: string | undefined

  @column({})
  public member_img_width: number

  @column({})
  public member_img_height: number

  @column({})
  public member_img_type: string

  @column({})
  public member_file_size: number

  @column({})
  public member_attach_name: string

  @column({})
  public role: string

  @column({})
  public is_deleted: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (tokens: User) {
    if (tokens.$dirty.password) {
      tokens.password = await Hash.make(tokens.password)
    }
  }
}
