import { DateTime } from 'luxon'
import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class AttachRelation extends BaseModel {
  @column({ isPrimary: true })
  public attach_relation_id: number

  @column({})
  public attach_id: number

  @column({})
  public entity_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

}
