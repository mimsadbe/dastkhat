import { DateTime } from 'luxon'
import {BaseModel, column, hasOne, HasOne} from '@ioc:Adonis/Lucid/Orm'
import Entity from "App/Models/Entity";

export default class EntityBlob extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public entity_id: number

  @column({})
  public summary: string

  @column({})
  public body: string

  @column({})
  public url_target: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(() => Entity)
  public entity: HasOne<typeof Entity>

}
