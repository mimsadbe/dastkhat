import { DateTime } from 'luxon'
import {BaseModel, column, ManyToMany, manyToMany} from '@ioc:Adonis/Lucid/Orm'
import Entity from "App/Models/Entity";

export default class Attach extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public attach_token: string

  @column({})
  public user_id: number

  @column({})
  public attach_title: string

  @column({})
  public attach_ext: string | undefined

  @column({})
  public attach_img_width: number

  @column({})
  public attach_img_height: number

  @column({})
  public attach_img_type: string

  @column({})
  public attach_file_size: number

  @column({})
  public attach_original_name: string

  @column({})
  public attach_priority: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @manyToMany(() => Entity, {
    localKey: 'id',
    pivotForeignKey: 'attach_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'entity_id',
    pivotTable: 'attach_relations',
  })
  public files: ManyToMany<typeof Entity>

}
