import { DateTime } from 'luxon'
import {BaseModel, column, ManyToMany, manyToMany} from '@ioc:Adonis/Lucid/Orm'
import Entity from "App/Models/Entity";


export default class Tag extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public title: string

  @column({})
  public user_id: number

  @column({})
  public tag_description: string

  @column({})
  public tag_total: number

  @column({})
  public is_deleted: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @manyToMany(() => Entity, {
    localKey: 'id',
    pivotForeignKey: 'tag_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'entity_id',
    pivotTable: 'tag_relations',
  })
  public category: ManyToMany<typeof Entity>
}
