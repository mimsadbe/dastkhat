import axios from "axios";
import {configs} from "../../configuration"

export function sendSmsNegin(text: string, mobile) {
  axios({
    method: 'POST',
    url: 'http://sms.3300.ir/api/wsSend.ashx',
    data: {
      username: configs.usernameNegin,
      password: configs.passwordNegin,
      line: configs.neginLine,
      mobile: mobile,
      message: text,
      life_time: 60,
      type: 0
    }
  })
    .then(res => {
      console.log(res.data)
    })
    .catch(error => {
      console.log(error);
    });
}

export function cleanObject(obj) {
  for (let propName in obj) {
    if ((obj[propName] === null) || (obj[propName] === 'null') || (obj[propName] === undefined) || (obj[propName] === 'undefined') || (obj[propName] === '')) {
      delete obj[propName];
    }
  }
  return obj
}
