import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Hash from '@ioc:Adonis/Core/Hash'
import User from 'App/Models/User'
import {rules, schema} from "@ioc:Adonis/Core/Validator";
import {sendSmsNegin, cleanObject} from "App/Fn";
import Application from "@ioc:Adonis/Core/Application";

const user = new User()
const sizeOf = require('image-size');

export default class UsersController {
  public async index ({request,response}: HttpContextContract) {

    const limit = request.input('limit',20)
    const page = request.input('page',1)
    let params = request.only(['username','email','mobile','gender','created_at'])
    let orderBy = request.input('order_by','id');
    let orderType = request.input('sort','desc');
    let hasImage = request.input('has_image',null)
    let search = request.input('q')

    params['is_deleted'] = false

    let has_image = {
      0:'attach_token = ""',
      1:'attach_token <> ""',
      null:''
    }

    params = cleanObject(params)

    try {
      const data =  await User
        .query()
        .where(params)
        .whereRaw(search?`name LIKE ${'%'+search+'%'} OR lastname LIKE ${'%'+search+'%'} OR email LIKE ${'%'+search+'%'}`:'')
        .whereRaw(has_image[hasImage]?has_image[hasImage]:'')
        .orderBy(orderBy,orderType)
        .paginate(page,limit)

      response.json({"success":true , data})

    }catch (e){
      return response.status(404).send('not found!')
    }

  }

  public async create ({auth,request,response}: HttpContextContract) {

    let params = request.only(["name","lastname","email","username","mobile","password","gender","role"])

    const newUser = schema.create({
      name: schema.string({escape:true,trim:true}, [rules.minLength(3)]),
      lastname: schema.string({escape:true,trim:true},[rules.minLength(2)]),
      email: schema.string({escape:true,trim:true},[rules.email(),rules.unique({ table: 'users', column: 'email' })]),
      username: schema.string({escape:true,trim:true},[rules.minLength(6),rules.unique({ table: 'users', column: 'username' })]),
      mobile: schema.string({escape:true,trim:true},[rules.mobile(),rules.minLength(11),rules.maxLength(11),rules.unique({ table: 'users', column: 'mobile' })]),
      password: schema.string({escape:true,trim:true},[rules.minLength(6)]),
      gender: schema.number(),
      role: schema.enum(['admin','member'] as const) ,
    })

    try {
      await request.validate({schema: newUser})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    // try {

      const avatar = request.file(`avatar`, {extnames: ['jpg', 'gif', 'png'],size: '2mb'})
      // return avatar?.clientName
      if (avatar){
        let now = new Date().getTime()
        let path = Application.tmpPath('uploads/users');
        if (avatar.type === 'image') {

          let fileName = `${now}-${Math.floor(100000 + Math.random() * 900000)}-${avatar.size}`
          let imgPath = `${fileName}.${avatar.extname}`
          await avatar.move(path , {name:imgPath})
          let dimensions = sizeOf(path+'/'+imgPath);

          user.attach_token = fileName
          user.member_attach_ext = avatar.extname
          user.member_img_width = dimensions.width
          user.member_img_height = dimensions.height
          user.member_img_type = avatar.type+'/'+avatar.subtype
          user.member_file_size = avatar.size
          user.member_attach_name = avatar.clientName
        }
      }


      user.name = params.name
      user.lastname = params.lastname
      user.fullname = user.name + ' ' +user.lastname
      user.email = params.email.toLowerCase()
      user.username = params.username
      user.mobile = params.mobile
      user.password = params.password
      user.gender = params.gender
      user.role = params.role

      let thisUser = await user.save()

      const token = await auth.use('api').generate(thisUser)

      response.json({"status":true , data:thisUser,token})

    // }catch (e){
    //   return response.status(400).send('error parameters')
    // }

  }

  public async store ({}: HttpContextContract) {
  }

  public async show ({request,response}: HttpContextContract) {

    let userId = await request.only(['id']);

    if(!userId.id){
      return response.json({"status": false, 'message': 'error params'})
    }

    try {
      const selectedUser = await User
        .query()
        .where({'is_deleted':'0','id':userId.id})

      let avatar
      if (selectedUser[0].attach_token){
        avatar  = '/users/avatar?id='+userId.id
      }

      if(selectedUser.length === 0){
        return response.status(404).json({"status":false , 'message': 'not found!'})
      }

      response.json({"status":true , data:selectedUser,avatar:avatar})
    }catch (e){
      return response.status(404).send('not found!')
    }

  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({request,response}: HttpContextContract) {

    let userId = await request.input('id');
    let params = request.only(["name","lastname","email","username","mobile","gender","role"])

    const userParam = schema.create({
      name: schema.string({escape:true,trim:true}, [rules.minLength(3)]),
      lastname: schema.string({escape:true,trim:true},[rules.minLength(2)]),
      email: schema.string({escape:true,trim:true},[rules.email()]),
      username: schema.string({escape:true,trim:true},[rules.minLength(6)]),
      mobile: schema.string({escape:true,trim:true},[rules.mobile(),rules.minLength(11),rules.maxLength(11)]),
      gender: schema.number(),
      role: schema.enum(['admin','member'] as const) ,
    })

    try {
      await request.validate({schema: userParam})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    let user = await User.findOrFail(userId);

    if (!user || user.is_deleted) {
      return response.status(404).json({status: 'error',message: "User not found!"})
    }

    try {
        await User
        .query()
        .where('id', userId)
        .update({name:params.name,lastname:params.lastname,fullname:user.name + ' ' +user.lastname,email:params.email,username:params.username,mobile:params.mobile,gender:params.gender,role:params.role})

        const avatar = request.file(`avatar`, {extnames: ['jpg', 'gif', 'png'],size: '2mb'})
        // return avatar?.clientName
        if (avatar){
          let now = new Date().getTime()
          let path = Application.tmpPath('uploads/users');
          if (avatar.type === 'image') {

            let fileName = `${now}-${Math.floor(100000 + Math.random() * 900000)}-${avatar.size}`
            let imgPath = `${fileName}.${avatar.extname}`
            await avatar.move(path , {name:imgPath})
            let dimensions = sizeOf(path+'/'+imgPath);

            await User
              .query()
              .where('id', userId)
              .update({attach_token:fileName,member_attach_ext:avatar.extname,member_img_width:dimensions.width,member_img_height:dimensions.height,member_img_type:avatar.type+'/'+avatar.subtype,member_file_size:avatar.size,member_attach_name:avatar.clientName})
          }
        }

        response.json({"status":true , message:'updated'})

    }catch (e){
      return response.status(400).send('error parameters')
    }

  }

  public async destroy ({request,response,auth}: HttpContextContract) {

    let id = request.only(["id"])

    let user = await User.find(id.id)

    if (!user || user.is_deleted) {
      return response.status(404).json({status: 'error',message: "User not found!"})
    }

    let this_user = await auth.authenticate()
    if (this_user.role != 'admin') {
      return response.status(403).json({status: 'error',message: 'Access Denied!'})
    }

    try {
      await User
        .query()
        .where('id', id.id)
        .update({ is_deleted: 1 })

      response.json({"status":true , message:"User successfully deleted!"})
    }catch{
      return response.status(404).send('Error in deleting user!')
    }

  }

  public async login ({auth, request, response}: HttpContextContract) {

    const username = request.input('username')
    const password = request.input('password')

    const userAuth = schema.create({
      username: schema.string(),
      password: schema.string(),
    })

    try {
      await request.validate({schema: userAuth})
    } catch (error) {
      return response.json({"status": false, "message": 'bad request'})
    }

    try {
      // Lookup user manually
      let user = await User
        .query()
        .whereRaw(`is_deleted = 0 and mobile='${username}' or email='${username}' or username='${username}'`)
        .firstOrFail()


      // Verify password
      if (!(await Hash.verify(user.password, password))) {
        // return response.badRequest('Invalid credentials')
        return response.json({"status": false, "message": 'username or password is wrong!'})
      }

      // Generate token
      const token = await auth.use('api').generate(user)

      await auth.login(user)

      return response.json({"status": true, "message": 'done', token, user})


    } catch (e) {
      return response.json({"status": false, "message": 'user not fount!'})
    }
  }

  public async logout ({auth, response}: HttpContextContract) {

    await auth.use('api').revoke()
    await auth.logout()

    return response.json({"status":true , "message":'logout done'})

  }

  public async forgetPass ({request,response}: HttpContextContract) {

    const { email,mobile } = request.all()
    let user;
    if (email){
      user = await User.findBy('email',email)
    }else if (mobile){
      user = await User.findBy('mobile',mobile)
    }

    if (!user || user.is_deleted) {
      return response.status(404).json({status: 'error',message: "Invalid Param!"})
    }

    let key = Math.floor(100000 + Math.random() * 900000);

    try {
      await User
        .query()
        .where('id',user.id)
        .update({forget_code:key,forget_sent_at:new Date()})

        // send sms
        const text = "کد تایید "+key;
        sendSmsNegin(text,user.mobile)

        return { message:'send key successfully!' }
    }catch (e){
      return response.status(500).json({status: 'error',message: "Error in sending forget key"})
    }
  }

  public async resetPass ({request,response}: HttpContextContract) {
    const { email, mobile,code, password } = request.all()
    let user;
    if (email){
      user = await User.findBy('email',email)
    }else if (mobile){
      user = await User.findBy('mobile',mobile)
    }
    if (!user || user.is_deleted || !user.forget_code) {
      return response.status(404).json({status: 'error',message: "Request not found!"})
    }

    let now = new Date()
    let diff = now.getTime() - user.forget_sent_at
    let diff_min = Math.floor(diff/ 60000);

    if (diff_min > 20) {
      return {message: "Your Request Expired!"}
    }
    if (code != user.forget_code) {
      return response.status(400).json({status: 'error',message: "Wrong Code!"})
    }

    try {

      let pass = await Hash.make(password)

      await User
        .query()
        .where('id',user.id)
        .update({password:pass,forget_code:null,forget_sent_at:null})

      return { message:'Password Reset Successfully.' }
    } catch (error) {
      console.log("Error in reset password: " , error);
      return response.status(500).json({status: 'error',message: "Error in reset password"})
    }

  }

  public async getImage ({request,response}: HttpContextContract) {
    let attachId = await request.input('id');
    let attach = await User.find(attachId)

    if (!attach) {
      return response.status(404).json({status: 'error',message: "image not found!"})
    }

    let fileName = attach.attach_token+'.'+attach.member_attach_ext ;

    let filePath = Application.tmpPath(`uploads/users/${fileName}`)

    return response.download(filePath)

  }

}
