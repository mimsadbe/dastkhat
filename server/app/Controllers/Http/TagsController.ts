import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {rules, schema} from "@ioc:Adonis/Core/Validator";
import Tag from "App/Models/Tag";
// import TagRelation from "App/Models/TagRelation";

const tag = new Tag()

export default class TagsController {
  public async index ({}: HttpContextContract) {
  }

  public async create ({auth,request,response}: HttpContextContract) {

    const user = await auth.authenticate()
    let param = request.only(['title','description'])

    const newTag = schema.create({
      title: schema.string({escape:true,trim:true}, [rules.unique({ table: 'tags', column: 'title' })]),
      description: schema.string.optional({escape:true})
    })

    const messages = {
      required: 'Please fill all fields!',
      unique: 'duplicate!'
    }
    try {
      await request.validate({schema: newTag,messages})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    try {

      tag.title = param.title
      tag.user_id = user.id
      tag.tag_description = param.description

      await tag.save()

    }catch {
      return response.status(400).json({"status":false , "message":"error params"})
    }

  }

  public async store ({}: HttpContextContract) {
  }

  public async show ({response,request}: HttpContextContract) {

    let tagId = await request.only(['id']);

    if(!tagId.id){
      return response.json({"status": false, 'message': 'error params'})
    }

    try {
      const tag = await Tag
        .query()
        .where({'is_deleted':'0','id':tagId.id})

      if(tag.length === 0){
        return response.status(404).json({"status":false , 'message': 'not found!'})
      }

      response.json({"status":true , tag})
    }catch (e){
      return response.status(404).send(e)
    }

  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({response,request}: HttpContextContract) {
    let tagId = await request.input('id');
    let params = request.only(['title','description'])

    const tagParam = schema.create({
      title: schema.string({escape:true,trim:true}),
      description: schema.string.optional({escape:true})
    })

    try {
      await request.validate({schema: tagParam})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    let tag = await Tag.findOrFail(tagId);

    if (!tag || tag.is_deleted) {
      return response.status(404).json({status: 'error',message: "tag not found!"})
    }

    try {
      await Tag
        .query()
        .where('id', tagId)
        .update({title:params.title,tag_description:params.description})

      response.json({"status":true , message:'updated'})

    }catch (e){
      return response.status(400).send('error parameters')
    }

  }

  public async destroy ({auth,request,response}: HttpContextContract) {
    let id = request.only(["id"])

    let tag = await Tag.find(id.id)

    if (!tag || tag.is_deleted) {
      return response.status(404).json({status: 'error',message: "tag not found!"})
    }

    let this_user = await auth.authenticate()
    if (this_user.role != 'admin') {
      return response.status(403).json({status: 'error',message: 'Access Denied!'})
    }

    try {
      await Tag
        .query()
        .where('id', id.id)
        .update({ is_deleted: 1 })

      response.json({"status":true , message:"tag successfully deleted!"})
    }catch{
      return response.status(404).send('Error in deleting tag!')
    }
  }
}
