import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {rules, schema} from "@ioc:Adonis/Core/Validator";
import Category from "App/Models/Category";


const category = new Category()

export default class CategoriesController {
  public async index ({}: HttpContextContract) {
  }

  public async create ({auth,request,response}: HttpContextContract) {
    const user = await auth.authenticate()
    let param = request.only(['title','description'])

    const newCategory = schema.create({
      title: schema.string({escape:true,trim:true}, [rules.unique({ table: 'categories', column: 'title' })]),
      description: schema.string.optional({escape:true})
    })

    const messages = {
      required: 'Please fill all fields!',
      unique: 'duplicate!'
    }
    try {
      await request.validate({schema: newCategory,messages})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    try {

      category.title = param.title
      category.user_id = user.id
      category.category_description = param.description

      await category.save()

    }catch {
      return response.status(400).json({"status":false , "message":"error params"})
    }

  }

  public async store ({}: HttpContextContract) {
  }

  public async show ({request,response}: HttpContextContract) {
    let categoryId = await request.only(['id']);

    if(!categoryId.id){
      return response.json({"status": false, 'message': 'error params'})
    }

    try {
      const category = await Category
        .query()
        .where({'is_deleted':'0','id':categoryId.id})

      if(category.length === 0){
        return response.status(404).json({"status":false , 'message': 'not found!'})
      }

      response.json({"status":true , category})
    }catch (e){
      return response.status(404).send(e)
    }

  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({request,response}: HttpContextContract) {

    let categoryId = await request.input('id');
    let params = request.only(['title','description'])

    const categoryParam = schema.create({
      title: schema.string({escape:true,trim:true}),
      description: schema.string.optional({escape:true})
    })

    try {
      await request.validate({schema: categoryParam})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    let category = await Category.findOrFail(categoryId);

    if (!category || category.is_deleted) {
      return response.status(404).json({status: 'error',message: "category not found!"})
    }

    try {
      await Category
        .query()
        .where('id', categoryId)
        .update({title:params.title,category_description:params.description})

      response.json({"status":true , message:'updated'})

    }catch (e){
      return response.status(400).send('error parameters')
    }

  }

  public async destroy ({auth,request,response}: HttpContextContract) {
    let id = request.only(["id"])

    let category = await Category.find(id.id)

    if (!category || category.is_deleted) {
      return response.status(404).json({status: 'error',message: "category not found!"})
    }

    let this_user = await auth.authenticate()
    if (this_user.role != 'admin') {
      return response.status(403).json({status: 'error',message: 'Access Denied!'})
    }

    try {
      await Category
        .query()
        .where('id', id.id)
        .update({ is_deleted: 1 })

      response.json({"status":true , message:"category successfully deleted!"})
    }catch{
      return response.status(404).send('Error in deleting category!')
    }
  }
}
