
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Entity from "App/Models/Entity";
import Attach from "App/Models/Attach";
import AttachRelation from "App/Models/AttachRelation";
import Application from '@ioc:Adonis/Core/Application'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import EntityBlob from "App/Models/EntityBlob";
// import Category from "App/Models/Category";
import CategoryRelation from "App/Models/CategoryRelation";
import TagRelation from "App/Models/TagRelation";
import Tag from "App/Models/Tag";


const entity = new Entity()
const blob = new EntityBlob()
const sizeOf = require('image-size');
// const category = new CategoryRelation()
// const tagRel = new TagRelation()

export default class EntitiesController {
  public async index ({request,response}: HttpContextContract) {
    const limit = request.input('limit',20)
    const page = request.input('page',1)

    try {
      const data =  await Entity
        .query()
        .where('is_deleted', '0')
        .where('is_confirmed', '1')
        .paginate(page,limit)

      response.json({"success":true , data})
    }catch (e){
      return response.status(404).send('not found!')
    }

  }

  public async create ({request,response,auth}: HttpContextContract) {

    let user = await auth.authenticate()

    let entityParams = request.only(["title", "subtitle", "priority", "is_confirmed","summary","body","url_target","category","tag"])
    // let images = request.files('attach')
    const images = request.files(`attach`, {extnames: ['jpg', 'gif', 'png'],size: '2mb'})

    const newEntity = schema.create({
      title: schema.string({escape:true,trim:true}, [rules.unique({ table: 'entities', column: 'title' })]),
      is_confirmed: schema.boolean(),
      // attach: schema.file.optional({
      //   size: '2mb',
      //   extnames: ['jpg', 'gif', 'png'],
      // }),

    })

    const messages = {
      required: 'Please fill all fields!',
      unique: 'duplicate!'
    }
    try {
      await request.validate({schema: newEntity,messages})
    } catch (error) {
      return response.json({"status": false, 'message': error.messages.errors })
    }

    let data = {}
    try {
      //--------------------------entity
      try {

        entity.title = entityParams.title
        entity.subtitle = entityParams.subtitle
        entity.priority = entityParams.priority
        entity.is_confirmed = entityParams.is_confirmed
        entity.user_id = user.id

        data['entity'] = await entity.save()
      }catch (e){
        return response.json({"status": false, 'message': e })
      }

      //--------------------------blob

      try {

        blob.summary = entityParams.summary
        blob.body = entityParams.body
        blob.url_target = entityParams.url_target

        blob.entity_id = entity.id
        data['blob'] = await blob.save()

      }catch (e){
        return response.json({"status": false, 'message': e })
      }

      // --------------------------category

      if (entityParams.category){
        const catIds = entityParams.category
        const catToArray = catIds.split(',')
        const cats = await Tag.findMany(catToArray)
        const arr = cats.map(item => ({'category_id': item.id, 'entity_id': entity.id}))
        await CategoryRelation.createMany(arr)
      }

      // --------------------------tag

      if (entityParams.tag){
        const tagIds = entityParams.tag
        const tagToArray = tagIds.split(',')
        const tags = await Tag.findMany(tagToArray)
        const arr = tags.map(item => ({'tag_id': item.id, 'entity_id': entity.id}))
        await TagRelation.createMany(arr)
      }

      // --------------------------image

      if (!images) {
        return
      }

      try {
      let i = 0;
      let now = new Date().getTime()
      let path = Application.tmpPath('uploads');
      for (let image of images) {
        i++
        let file = new Attach()
        if (image.type === 'image') {
            let fileName = `${now}-${entity.id}-${Math.floor(100000 + Math.random() * 900000)}-${image.size}`
            let imgPath = `${fileName}.${image.extname}`
            await image.move(path , {name:imgPath})
            let dimensions = sizeOf(path+'/'+imgPath);


            // await move(Application.tmpPath('uploads'))

            file.attach_title = entity.title
            file.attach_ext = image.extname
            file.attach_original_name = image.clientName
            file.attach_file_size = image.size
            file.attach_img_type = image.type+'/'+image.subtype
            file.attach_img_width = dimensions.width
            file.attach_img_height = dimensions.height
            file.attach_token = fileName
            file.attach_priority = i
            file.user_id = user.id
            await file.save()
            //---------------------------------image_relation
            let attachRelation = new AttachRelation()
            attachRelation.entity_id = entity.id
            attachRelation.attach_id = file.id

            await attachRelation.save()

            if (i == 1) {
              await Entity
              .query()
              .where('id',entity.id)
              .update('index_attach_id',attachRelation.attach_relation_id)
            }
          }
        }
      }catch (e){
        console.log(e);
      }

      response.json({"status": true, data})

    }catch (e) {
      return response.json({
        success: false,
        message: 'unexpected error'
      })
    }
  }

  public async store ({}: HttpContextContract) {

  }

  public async show ({request,response}: HttpContextContract) {
    let entityId = await request.only(['id']);

    if(!entityId.id){
      return response.json({"status": false, 'message': 'error params'})
    }

    try {
      const entity = await Entity.query().preload('blob').where('id',entityId.id).preload('files').preload('category').preload('tag')

      if(entity.length === 0){
        return response.status(404).json({"status":false , 'message': 'not found!'})
      }

      response.json({"status":true , entity})
    }catch (e){
      return response.status(404).send('not found!')
    }

  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({request,response}: HttpContextContract) {
    try {
      //--------------------------entity
      let entityId = await request.input('id');
      let entityParams = request.only(["title","subtitle","priority"])

      const entity = await Entity.findOrFail(entityId)

      for (const key in entityParams) {
        entity[key] = entityParams[key]
      }

      await entity.save()

      //--------------------------blob

      let entityBlob = request.only(["summary","body","url_target"])

      await EntityBlob
        .query()
        .where('entity_id', entityId)
        .update({summary:entityBlob.summary,body:entityBlob.body,url_target:entityBlob.url_target})


      //--------------------------image
      const images = request.files('attach')

      if (!images) {
        return
      }

      let i=0;
      for (let image of images) {
        i++
        let attach = new Attach()
        if (image.type === 'image'){
          await image.move(Application.tmpPath('uploads'))
          attach.attach_title = entity.title
          attach.attach_ext = image.extname
          attach.attach_original_name = image.clientName
          attach.attach_priority = i
          await attach.save()
          // ---------------------------------image_relation
          let attachRelation = new AttachRelation()
          attachRelation.entity_id = entityId.id
          attachRelation.attach_id = entityId.attach_id

          await attachRelation.save()

          if (i===1){
            entity.index_attach_id = attachRelation.attach_relation_id
          }
        }
      }

    //   await entity.save()
      response.json({"success":true , entity})
    //
    } catch (e){
      return response.json({
        success: false,
        message: 'unexpected error'
      })
    }
  }

  public async destroy ({request,response,auth}: HttpContextContract) {

    let id = request.only(["id"])

    let entity = await Entity.find(id.id)

    if (!entity || entity.is_deleted) {
      return response.status(404).json({status: 'error',message: "Entity not found!"})
    }

    let this_user = await auth.authenticate()
    if (this_user.role != 'admin') {
      return response.status(403).json({status: 'error',message: 'Access Denied!'})
    }

    try {
      await Entity
        .query()
        .where('id', id.id)
        .update({ is_deleted: 1 })

      response.json({"success":true , "message":"entity successfully deleted!"})
    }catch (e){
      return response.status(404).send('Error in deleting entity!')
    }

  }

  public async getImage ({request,response}: HttpContextContract) {
    let attachId = await request.input('id');
    let attach = await Attach.find(attachId)

    if (!attach) {
      return response.status(404).json({status: 'error',message: "image not found!"})
    }

    let fileName = attach.attach_token+'.'+attach.attach_ext ;

    let filePath = Application.tmpPath(`uploads/${fileName}`)

    return response.download(filePath)

  }

  public async getImageIndex ({request,response}: HttpContextContract) {

    let entityId = await request.input('id');
    let entity = await Entity.find(entityId)

    if (!entity || entity.is_deleted) {
      return response.status(404).json({status: 'error',message: "entity not found!"})
    }

    if (isNaN(entity.index_attach_id)) {
        return response.status(404).json({status: 'error',message: "Image not found!"})
    }

    let attach = await Attach.find(entity.index_attach_id)

    // let fileName = attach?.attach_original_name ;
    // let filePath = Application.tmpPath(`uploads/${fileName}`)
    // return response.download(index)

    let index = '/entities/image?id='+attach?.id

    response.json({"status":true , index})
  }

  public async getImageAlbum ({request,response}: HttpContextContract) {
    let entityId = await request.input('id');
    let entity = await Entity.find(entityId)

    if (!entity || entity.is_deleted) {
      return response.status(404).json({status: 'error',message: "entity not found!"})
    }

    let album = await Entity.query().preload('files').where('id',entityId)

    let images = album[0]['files'].map(item=>{
      return '/entities/image?id='+item.id
    })

    response.json({"status":true , images})

  }

}
