import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Admin {
  public async handle ({auth , response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const user = await auth.authenticate()
    if ((user.role != 'admin' || user.is_deleted)) {
      return response.status(403).json({status: 'error',message: 'Access Denied! You are not admin!'})
    }
    await next()
  }
}
