import Input from "/component/form/input/Input";
import Checkbox from "/component/form/checkbox/Checkbox";
import {useState} from "react";
import Router from 'next/router'
import Link from "next/link";
import {ToastContainer, toast} from "react-toastify";
import {useCookies} from 'react-cookie';

const login = () => {

    const [dataEdit, setDataEdit] = useState({});

    // ---
    const [cookies, setCookie] = useCookies(['user']);
    // ---

    const successMessage = (text, data) => {
        toast.success(text)
        Router.push('/dashboard')
        // console.log(data);
    };

    const badRequest = (text, status) => {
        toast.error(text)
    };

// ---
    function handelForm(key, value) {
        let _dataEdit = dataEdit;
        _dataEdit[key] = value;
        setDataEdit(_dataEdit);
    }

// --

    const sendDta = async (e) => {
        e.preventDefault()
        let resp = await fetch('http://127.0.0.1:3333/login', {
                method: 'POST',
                headers: {'Content-Type': 'application/json',},
                body: JSON.stringify(dataEdit),
            }
        )
        if (resp) {
            const cleanResponse = await resp.json()
            console.log(cleanResponse.success = "true")
            console.log(cleanResponse)
            if (cleanResponse.success === "true") {
                successMessage("welcome")
                setCookie("user",cleanResponse.user)
            }else{
                badRequest("Not Found !!!")
            }
        }
        //     .then((res) => {
        //         if (res.ok) {
        //             successMessage(res.statusText, res.json())
        //             // setCookie()
        //             // console.log();
        //         } else {
        //             badRequest(res.statusText)
        //         }
        //     }
        // )
    }


    return (
        <div className="login_page">
            <ToastContainer/>
            <div className="login_box">
                <form method={"get"} onSubmit={(e) => sendDta(e)}>
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "نام کاربری",
                                name: "username",
                                info: "* نام کاربری ایمیل شماست",
                                validator: ["isEmpty", "isEmail"],
                                invalid_message: ["خالی نذار !!", "این چه طرز وارد کردن ایمیله !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "پسورد",
                                name: "password",
                                type: "password",
                                validator: ["isEmpty"],
                                invalid_message: ["پسوردش کو ؟؟؟"],
                            },
                        ]}
                    />
                    <Checkbox title_label="مرا به خاطر بسپار" onChange={(value, name) => {
                        handelForm("confirm", value)
                    }}/>
                    <div className="d-flex justify-content-between">
                        <input type="submit" value={'ورود'} className="btnsubmit"/>
                        <Link href={"/sign_in"}>
                            <a className="btnsubmit">ثبت نام</a>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default login;
