import { useState, useContext } from "react";
import { notification } from "../provider/notification";

export default function Home() {
  // ----
  const [input, setInput] = useState();
  const [checkbox, setCheckbox] = useState();
  const [radio, setRadio] = useState();
  const [textarea, setTextarea] = useState();
  const a = useContext(notification);
  // ----
  const functionThatReturnPromise = () =>
  new Promise((resolve) => {
      // e.preventDefault()
      fetch('http://localhost:3000/api/users', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({username: 'sadegh@gmail.com', password: '123456'}),
      })
  });
  
  function changeNotif() {
   if(a){
       a.setNotif(functionThatReturnPromise);
   }
  }
  console.log(a);
  return <div onClick={changeNotif}> hi </div>;
}
