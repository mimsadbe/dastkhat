import Input from "/component/form/input/Input";
import Checkbox from "/component/form/checkbox/Checkbox";
import Radio from "/component/form/radio/Radio";
import { useState } from "react";

  //---

export default function Home() {
  const [dataEdit, setDataEdit] = useState({});
  // ---
  function handelForm(key, value) {
    let _dataEdit = dataEdit;
    _dataEdit[key] = value;
    setDataEdit(_dataEdit);
  }
  // --
  return (
    <>
      <div className="edit_page">
        <Checkbox
          title_label="نیاز به برسی"
          name="600"
          onChange={(value, name) => {handelForm("confirm",value)}}
        />
        <Input
          data={[{ title_label: "عنوان", name: "title" }]}
          onChange={(value, name) => {handelForm(name, value);}}
        />
        <Input
          data={[{ title_label: "slug", name: "slug" }]}
          onChange={(value, name) => {handelForm(name, value);}}
        />
        <Input
          data={[{ title_label: "کلمات بهبود جستجو", name: "metadata" }]}
          onChange={(value, name) => {handelForm(name, value);}}
        />
        <Input
          data={[{ title_label: "خلاصه", name: "kholase" }]}
          onChange={(value, name) => {handelForm(name, value);}}
        />
        <Radio
          title_label={"مناسب برای :"}
          className="ssdsdsd"
          name={"age_user"}
          data={[
            { value: "2", label: "۶تا ۱۸ سال" },
            { value: "1", label: "۱ساله تا ۶ ساله" },
            { value: "3", label: "۱۸ تا ۲۵" },
            { value: "4", label: "۲۵ تا ۳۰" },
            { value: "5", label: "۳۰ تا ۴۰" },
            { value: "6", label: "۳۰ تا ۴۰" },
            { value: "7", label: "۳۰ تا ۴۰" },
            { value: "8", label: "۳۰ تا ۴۰" },
          ]}
          onChange={(value) => {
            let _dataEdit = dataEdit;
            _dataEdit.age_user = value;
            setDataEdit(_dataEdit);
          }}
        />
      </div>
    </>
  );
}
