import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "../styles/globals.css";
import {useRouter} from "next/router";
import Head from "next/head";
import {useState, useEffect,createContext} from "react";
import DashboardNavbar from "/component/dashboardnavbar/DashboardNavbar";
import AsideDashboard from "/component/asideDashboard/AsideDashboard";
import { CookiesProvider } from "react-cookie";
import {ToastContainer, toast} from "react-toastify";


function MyApp({Component, pageProps}) {

    const [notif, setNotif] = useState();

    // const notification = createContext();
    // console.log(notif);

    if (notif) {
        toast.promise(notif, {
            pending: "Promise is pending",
            success: "Promise resolved 👌",
            error: "Promise rejected 🤯",
        });
    }


    const router = useRouter();
    if (router.asPath.split("/")[1] == "dashboard") {
        return (
            // <notification.Provider value={{notif, setNotif}}>
            <CookiesProvider>
                <div className="d-flex">
                    <ToastContainer/>
                    <Head>
                        <title>HOJRE DASHBOARD</title>
                        <meta name="description" content="dashbord"/>
                        <link rel="icon" href="/favicon.ico"/>
                    </Head>
                    <AsideDashboard
                        data={[
                            {
                                title: "ثبت محتوا",
                                child: [
                                    {childTitle: "ثبت بلاگ", href: "/test2"},
                                    {childTitle: "ثبت نویسنده", href: "/test1"},
                                    {childTitle: "ثبت ناشر", href: "/test3"},
                                ],
                                image: "/img/file.svg",
                            },
                            {
                                title: "لیست محتوا",
                                child: [
                                    {childTitle: "لیست بلاگ", href: "/dashboard/edit"},
                                    {childTitle: "لیست نویسنده", href: "/test5"},
                                    {childTitle: "لیست ناشر", href: "/test7"},
                                ],
                                image: "/img/file.svg",
                            },
                            {
                                title: "مدیران",
                                child: [
                                    {childTitle: "لیست ادمین", href: "/dashboard/admin"}
                                ],
                                image: "/img/file.svg",
                            },
                        ]}
                    />
                    <main className="main">
                        <DashboardNavbar/>
                        <section className="container">
                            <form
                                className="w-100"
                                // action="https://formspree.io/f/mdoybkzq"
                                // method="POST"
                            >
                                <Component {...pageProps} />
                            </form>
                        </section>
                    </main>
                </div>
            </CookiesProvider>
            // </notification.Provider>
        );
    } else {
        return <Component {...pageProps} />;
    }
}

export default MyApp;
