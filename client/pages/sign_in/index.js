import Input from "/component/form/input/Input";
import Radio from "/component/form/radio/Radio";
import {useState} from "react";
import Link from "next/link";
import {ToastContainer, toast} from 'react-toastify';

const sign_in = () => {
    const [dataEdit, setDataEdit] = useState({
        "role": "member"
    });



    const successMessage = (text,status) => {
        toast.success(text)
    };

    const badRequest = (text,status) => {
        toast.error(text)
    };

    function handelForm(key, value) {
        let _dataEdit = dataEdit;
        _dataEdit[key] = value;
        setDataEdit(_dataEdit);
    }

// --

    const sendDta = (e) => {
        console.log(dataEdit);
        e.preventDefault();
        fetch('http://127.0.0.1:3333/users/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataEdit),
        }).then((res) => {

            // console.log(res.statusText);
            // console.log(responseMessage);

            if (res.ok) {
                successMessage(res.statusText)
                } else {
                badRequest(res.statusText)
                }

            }
        )
    }

    return (
        <div className="login_page">
            <ToastContainer/>
            <div className="login_box">
                <form method={"get"} onSubmit={(e) => sendDta(e)}>
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "نام",
                                name: "name",
                                validator: ["isEmpty"],
                                invalid_message: ["خالی نذار !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "نام خانوادگی",
                                name: "lastname",
                                validator: ["isEmpty"],
                                invalid_message: ["خالی نذار !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "نام کاربری",
                                name: "username",
                                validator: ["isEmpty"],
                                invalid_message: ["خالی نذار !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "ایمیل",
                                name: "email",
                                validator: ["isEmpty", "isEmail"],
                                invalid_message: ["خالی نذار !!", "این چه طرز وارد کردن ایمیله !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "شماره",
                                name: "mobile",
                                validator: ["isEmpty"],
                                invalid_message: ["خالی نذار !!"],
                            },
                        ]}
                    />
                    <Input
                        onChange={(value, name) => {
                            handelForm(name, value);
                        }}
                        data={[
                            {
                                title_label: "پسورد",
                                name: "password",
                                type: "password",
                                validator: ["isEmpty"],
                                invalid_message: ["پسوردش کو ؟؟؟"],
                            },
                        ]}
                    />
                    <Radio
                        title_label={"جنسیت :"}
                        className="ssdsdsd"
                        name={"gender"}
                        data={[
                            {value: "0", label: "آقا"},
                            {value: "1", label: "خانم"},
                        ]}
                        onChange={(value, name) => {
                            handelForm("gender", value);
                            console.log(value);
                        }}
                    />
                    <div className="d-flex justify-content-between">
                        <input type="submit" value={'ثبت'} className="btnsubmit"/>
                        <Link href={"/login"}>
                            <a className="btnsubmit">ورود</a>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default sign_in