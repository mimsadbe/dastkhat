import { useState } from "react";
import style from "/styles/Form.module.css";
var validator = require("validator");
/**
 * @param {*} props
 *
 *
 *
 * @returns
 */

// --------------------------
const Radio = (props) => {
  function handleChange(event) {
    console.log(event.target.checked);
  }
  return (
    <label className={props.className || style.custom_radio_box}>
      <span className={style.custom_radio_label}>{props.title_label}</span>
      <div className={style.custom_radio}>
        <input
          type="radio"
          name={props.name}
          className={style.radio}
          onChange={handleChange}
        />
        <span className={style.custom_radio_ncheck}></span>
        <img
          className={style.custom_radio_icheck}
          src="/img/checked.svg"
          width="24"
          height="24"
        />
      </div>
    </label>
  );
};

// --------------------------
const Textarea = (props) => {
  function handleChange(event) {
    props.onChange(event.target.value);
    console.log(event.target.value);
  }
  return (
    <label className={props.className || style.custom_textarea_box}>
      <span className={style.custom_textarea_label}>{props.title_label}</span>
      <textarea
        id={props.id || ""}
        className={style.custom_textarea}
        type={props.type || ""}
        placeholder={props.placeholder || ""}
        name={props.name || ""}
        onChange={handleChange}
      >
        {props.text}
      </textarea>
    </label>
  );
};

export { Radio, Textarea };
