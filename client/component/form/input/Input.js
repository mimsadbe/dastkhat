import {useState,useRef} from "react";

// ---
var validator = require("validator");
// ---

const Input = (props) => {

    const [dataInput, setDataInput] = useState("");

    function handleChange(value, name) {
        setDataInput(value)
        props.onChange(value, name)

    }

    const [isValid, setIsValid] = useState({status: "", message: ""});

    const items = props.data.map((data, index) => {
        function validation() {
            var valOption = data.validator;
            var valmessage = data.invalid_message;
            for (let i in valOption) {
                if (valOption[i] == "isEmpty" && validator.isEmpty(dataInput)) {
                    setIsValid({status: false, message: valmessage[i] || "نامعتبر"});
                    return false
                }
                if (valOption[i] == "isEmail" && !validator.isEmail(dataInput)) {
                    setIsValid({status: false, message: valmessage[i] || "نامعتبر"});
                    return false
                }


            }
            setIsValid({status: true, message: ""});
        }

        return (
            <>
                <div
                    className={`${
                        data.className ? data.className : ""
                    } custom_input_box ${isValid.status === false ? "not_valid" : ""}`}
                >
                    <label className="custom_label_box" key={index}>
                        <input
                            id={data.id || ""}
                            className="custom_input"
                            type={data.type || ""}
                            placeholder={data.placeholder || ""}
                            onBlur={validation}
                            value={dataInput}
                            onChange={(e) => handleChange(e.target.value, data.name)}
                            name={data.name || ""}
                        />
                        <span
                            className={`${
                                dataInput.length > 0  ? "active" : ""
                            } custom_input_label`}
                        >
              {data.title_label}
            </span>
                    </label>
                    {isValid.status == false ? (
                        <div className="not_valid_message"> {isValid.message}</div>
                    ) : (
                        ""
                    )}
                    {data.info ? <span className="custom_info"> {data.info} </span> : ""}
                </div>
            </>
        );
    });

    return items;
};

export default Input;
