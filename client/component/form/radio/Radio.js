import { useState } from "react";

// {(props.className ? className={props.className} : "")}
const Radio = (props) => {
    const [dataInput, setDataInput] = useState("");

    function handleChange(event,name) {
        setDataInput(event.target.value)
        props.onChange(event.target.value,name)
    }

  const items = props.data.map((data, index) => {
    return (
      <label key={index} >
        <div className="custom_radio">
          <input
            type="radio"
            name={props.name}
            className="radio"
            onChange={(e)=>handleChange(e,data.label || "")}
            // onChange={(e) => {props.onChange({value:e.target.value,title:data.label})}}
            value={data.value}
          />
          <span className="custom_radio_ncheck">{data.label}</span>
          {/* <img
          className={custom_radio_icheck}
          src="/img/checked.svg"
          width="24"
          height="24"
        /> */}
        </div>
      </label>
    );
  });
  return (
    <div className="custom_radio_box">
      <span className="custom_radio_label">{props.title_label}</span>
      {items}
    </div>
  );
};

export default Radio;
