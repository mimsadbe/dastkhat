const Checkbox = (props) => {
  function handleChange(event,name) {
    props.onChange(event.target.checked,name)
  }
  return (
    <label
      className={`${
        props.className ? props.className : ""
      } custom_checkbox_box`}
    >
      <div className="custom_checkbox">
        <input
          type="checkbox"
          name={props.name}
          className="checkbox"
          onChange={(e)=>handleChange(e,props.name)}
        />
        <span className="custom_checkbox_ncheck"></span>
        {/* <img
          className="custom_checkbox_icheck"
          src="/img/checked.svg"
          width="24"
          height="24"
        /> */}
      </div>
      <span className="custom_checkbox_label">{props.title_label}</span>
    </label>
  );
};

export default Checkbox;
