import { Button, UncontrolledCollapse } from "reactstrap";
import Link from "next/link";
import { useState } from "react";

function toggleDropDown(item, index) {
  const [collapse, setCollapse] = useState(false);

  return (
    <>
      <div>
        <Button
          id={`aside_item_${index}`}
          className={`btn_aside ${collapse ? "down" : "up"}`}
          color=""
          onClick={() => {
            setCollapse(!collapse);
          }}
          key={index}
        >
          <div className="btn_aside_icon">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <line x1="3" y1="12" x2="21" y2="12"></line>
              <line x1="3" y1="6" x2="21" y2="6"></line>
              <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg>
          </div>
          <div className="btn_aside_text">{item.title}</div>
        </Button>
        <UncontrolledCollapse toggler={`#aside_item_${index}`}>
          <div className="card_aside">
            {item.child.map((_item, _index) => (
              <Link href={_item.href}>{_item.childTitle}</Link>
            ))}
          </div>
        </UncontrolledCollapse>
      </div>
    </>
  );
}

const AsideDashboard = (props) => {
  return (
    <aside className="aside">
      <div className="header_aside">
        <img src="/img/almalogo.png" alt="hojre" />
      </div>
      <div className="content_aside">
        {props.data.map((item, index) => {
          return toggleDropDown(item, index + 1);
        })}
      </div>
    </aside>
  );
};

export default AsideDashboard;
