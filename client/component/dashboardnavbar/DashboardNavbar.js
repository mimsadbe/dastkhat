import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledButtonDropdown,
} from "reactstrap";
import {useCookies} from 'react-cookie';
import router from "next/router";


const DashboardNavbar = () => {

  // ---
  const [cookies, setCookie, removeCookie] = useCookies(['user']);
  // ---
   function logout(){
     removeCookie("user")
     router.push("/")
  }
  // ---
  return (
    <div className="container">
      <nav className="navbar">
        <UncontrolledButtonDropdown>
          <DropdownToggle caret className="dropdown" color="">
            {cookies.user ? cookies.user.fullname : "کاربر"}
          </DropdownToggle>
          <DropdownMenu>
            {/*<DropdownItem header>Header</DropdownItem>*/}
            {/*<DropdownItem disabled>Action</DropdownItem>*/}
            <DropdownItem href="/profile">
              پروفایل
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem className="btn-danger" onClick={()=>{logout()}}>خروج</DropdownItem>
          </DropdownMenu>
        </UncontrolledButtonDropdown>
      </nav>
    </div>
  );
};

export default DashboardNavbar;
